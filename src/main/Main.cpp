/*
Author: Alex Wilson <Dk789@live.missouristate.edu>
		Cody Hart <cph2013@live.missouristate.edu>
		*/


#include <iostream>
#include <string>

bool isWord(std::string);
bool strIsInL(std::string);

int main() {
    std::cout << "$" << std::endl;
	std::cout << "$$" << std::endl;
	std::cout << "$$$" << std::endl;
	std::cout << "$$$$" << std::endl;
	std::cout << "$$$$$" << std::endl;
	std::cout << "$$$$$$" << std::endl;
	std::cout << "$$$$$$$" << std::endl;
	std::cout << "abb" << std::endl;
	std::cout << "aabbbb" << std::endl;
	std::cout << "$abb" << std::endl;
	std::cout << "$aabbbb" << std::endl;
	std::cout << "$$abb" << std::endl;
	std::cout << "$$$abb" << std::endl;
	std::cout << "$$$$abb" << std::endl;
    return 0;
	
}

/**
 * This function is a solution to Exercise 6, Chapter 5.
 * @param word a potential word in the language described in Exercise 6
 * @return true if the string is in the given language, false otherwise.
 */
bool isWord(std::string word) {
	
	char firstChar = word[0];
	int changeSize = word.size();
	int staySize = word.size();
	char lastChar = word[staySize - 1];
	
	if(lastChar == '.') {
		if (changeSize > 1 && (firstChar == '-' || firstChar == '.')) {
			return isWord(word.substr(1,changeSize - 1));
			}
		else if(changeSize == 1 && (firstChar == '-' || firstChar == '.')) {
			return true;
			}
	}
	
	
	return false;
	
}

/**
 * This function is a solution to Exercise 9, Chapter 5.
 * @param s a potential string in language L
 * @return true if the given string is valid in L, false otherwise
 */
int i = 1;
bool strIsInL(std::string s) {
	int size = s.size();
	char firstChar = s.at(0);
	std::string lastTwoChar = s.substr(size - 2, 2);
	
	if (size < 3) {
		return false;
	}
	if (s == "ABB"){
		 return true;
	}
	else if (firstChar == 'A' &&  lastTwoChar == "BB" ){
		return strIsInL(s.substr(1, size - 3));
	}  
	return false;
}